<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\base\Exception;
use yii\console\Controller;
use yii\console\ExitCode;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
//use Facebook\WebDriver\Remote\Cookie;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class WebdriverController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {
        $host = 'http://localhost:4444/wd/hub';

        $ch = curl_init($host);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
        $return = curl_exec($ch);
        if (empty($return)){
            echo "run selenium-server-standalone $host\n";
            return ExitCode::UNSPECIFIED_ERROR;
        } else {
            echo "running selenium-server-standalone ...\n";
        }

        // this is the default
        $capabilities = DesiredCapabilities::chrome();
        $driver = RemoteWebDriver::create($host,$capabilities,1000000,1000000);

        $url = 'https://tass.ru/search?searchStr=ГЧП&sort=date';
        echo "get page $url\n";
        $driver->get($url);

        $driver->quit();

        return ExitCode::OK;
    }
}
